package com.example.part1.demopart1.models;

public class HelloWorld {
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public void getMessage() {
        System.out.println("Print : " + message);
    }
}