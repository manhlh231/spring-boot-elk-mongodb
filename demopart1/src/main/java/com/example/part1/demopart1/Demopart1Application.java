package com.example.part1.demopart1;

import com.example.part1.demopart1.models.HelloWorld;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;

@SpringBootApplication
public class Demopart1Application {

	public static void main(String[] args) {
		SpringApplication.run(Demopart1Application.class, args);

		DefaultListableBeanFactory factory = new DefaultListableBeanFactory();

		XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);

		reader.loadBeanDefinitions(new ClassPathResource("bean.xml"));


		HelloWorld obj = (HelloWorld) factory.getBean("helloWorld");

		obj.getMessage();
	}

}
