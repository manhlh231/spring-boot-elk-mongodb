package com.example.part1.demopart1.database;

import com.example.part1.demopart1.models.Product;
import com.example.part1.demopart1.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Database {
    private static final Logger logger = LoggerFactory.getLogger(Database.class);
    @Bean
    CommandLineRunner initDatabase(ProductRepository productRepository){
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
//                Product p1 =  new Product("Product 1", 2022, 900.0);
//                Product p2 =  new Product("Product 2", 2022, 700.0);
//                logger.info("insert data: " + productRepository.save(p1) );
//                logger.info("insert data: " +  productRepository.save(p2));
            }
        };
    }
}
