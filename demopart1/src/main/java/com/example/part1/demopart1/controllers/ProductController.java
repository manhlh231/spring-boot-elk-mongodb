package com.example.part1.demopart1.controllers;

import com.example.part1.demopart1.models.Product;
import com.example.part1.demopart1.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/Products")
public class ProductController {
    // DI
    @Autowired
    private ProductRepository productRepository;

    @GetMapping("")
    List<Product> getAllProduct(){
        return productRepository.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity<ResponseObject> getDetailProduct(@PathVariable Long id){

        Optional<Product> product = productRepository.findById(id);

        return product.isPresent() ?
             ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("ok", "Get product success", product))
        :
             ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject("error", "can not find with id: "+id, ""));


    }

    @PostMapping("/insert")
    ResponseEntity<ResponseObject> insertProduct(@RequestBody Product product){

        List<Product> products = productRepository.findByName(product.getName().trim());
        if(products.size() > 0){
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(new ResponseObject("failed", "Product name already taken", ""));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("ok", "ínsert product success", productRepository.save(product)));
    }

    @PutMapping("/update/{id}")
    ResponseEntity<ResponseObject> updateProduct(@RequestBody Product product, @PathVariable Long id){
        Product oldProduct = productRepository.findById(id).map(e ->{
                e.setName(product.getName());
                e.setPrice(product.getPrice());
                e.setYear(product.getYear());
               return productRepository.save(product);
            }).orElseGet(() ->{
                product.setId(id);
               return productRepository.save(product);
            });
        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "update product success", oldProduct)
        );
    }

    @DeleteMapping("/delete")
    ResponseEntity<ResponseObject> deleteProduct(@PathVariable Long id){
        Boolean isProduct = productRepository.existsById(id);
        if(isProduct){
            return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Delete product success", "")
            );
        }else{
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                    new ResponseObject("failed", "Product is not exists", "")
            );
        }


    }
}
